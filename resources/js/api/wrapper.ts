import axios from 'axios';

const csrfCookieName = 'XSRF-TOKEN'
const instance = axios.create({
  baseURL: '',
  timeout: 10000,
  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-CSRF-TOKEN',
  withCredentials: true,
  headers: {
    "Content-Type": "application/json"
  },
  validateStatus: function(status: number) {
    return (status >= 200 && status < 300);
  }
});

// const exeptionAutoErrorEndpoints = [/\/me$/];
// const exeptionAutoError = ["resource-not-found"];

// instance.interceptors.response.use(
//   (res: any) => {
//     //OnEverySuccess
//     return res;
//   },
//   (err: any) => {
//     //OnEveryError

//     // Exclude les endpoint présent dans exeptionAutoErrorEndpoints
//     for (const exclude of exeptionAutoErrorEndpoints) {
//       if (exclude.test(err.response.config.url)) {
//         return Promise.reject(err);
//       }
//     }

//     let text = "";
//     if (err.response.data.meta) {
//       for (const errorField in err.response.data.meta) {
//         text += errorField + " : ";
//         for (const errorMeta of err.response.data.meta[errorField]) {
//           text += errorMeta + " ";
//         }
//       }
//     }
//     for (const errorKey in err.response.data.errors) {
//       if (exeptionAutoError.includes(errorKey)) continue;
//       Store.dispatch("notif/push", {
//         level: NOTIF_LEVEL.ERROR,
//         text:
//           err.response.data.errors[errorKey].details +
//           (text ? " \n> " + text + " ) " : ""),
//         active: true,
//         id: null
//       });
//     }

//     return Promise.reject(err);
//   }
// );

function isCSRFCookiePresent() {
  return document.cookie.includes(csrfCookieName + '=')
} 
const API = {
  state: {
    getState() {
      return new Promise((resolve, reject) => {
        console.log("State/getState START")
        instance
        .get("/status")
        .then((result) => {
          console.log("State/getState SUCCESS : ", result)
          resolve(result.data)
        })
        .catch((err: any) => {
          console.warn("State/getState FAIL : ", err)
          reject(err);
        });
      });
    }
  }
};

export default API;
