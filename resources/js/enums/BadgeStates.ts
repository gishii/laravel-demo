export default Object.freeze({
    Neutral : 'neutral',
    Danger :'danger',
    Info : 'info',
    Success : 'success',
    Warning : 'warning',
})
