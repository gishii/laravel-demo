<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Facades\Log;

class TestJob implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(
        private readonly int $timeInMilliseconds,
    ) {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        usleep($this->timeInMilliseconds * 1000);

        Log::info("Job completed after {$this->timeInMilliseconds} milliseconds");
    }
}
