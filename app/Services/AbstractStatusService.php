<?php

namespace App\Services;

use Exception;

abstract class AbstractStatusService
{
    public function status(): bool
    {
        return !$this->accessTry() instanceof Exception;
    }

    public function output(): ?string
    {
        $result = $this->accessTry();

        if (empty($result)) {
            return null;
        }

        return $result instanceof Exception
            ? $result->getMessage()
            : $result;
    }

    abstract public function name(): string;

    abstract public function currentDefaultDriver(): string;

    abstract protected function accessTry(): string|Exception|null;
}
