<?php

namespace App\Services;

use App\Services\Concerns\TestRedis;
use Exception;
use Illuminate\Support\Facades\Queue;

class QueueStatusService extends AbstractStatusService
{
    use TestRedis;

    public function name(): string
    {
        return 'queue';
    }

    public function currentDefaultDriver(): string
    {
        return Queue::getDefaultDriver();
    }

    protected function accessTry(): string|Exception|null
    {
        return match ($this->currentDefaultDriver()) {
            'redis' => $this->redisAccessTry(),
            default => null,
        };
    }
}
