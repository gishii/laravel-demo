<?php

namespace App\Services\Concerns;

use Exception;
use Illuminate\Support\Facades\Redis;

trait TestRedis
{
    protected function redisAccessTry(): string|Exception|null
    {
        try {
            return Redis::command('INFO')['redis_version'] ?? 'unknown version';
        } catch (Exception $e) {
            return $e;
        }
    }
}
